import Home from './components/Home';
import { BrowserRouter, Switch, Route, RouteComponentProps } from 'react-router-dom';
import "./App.css"
import SearchView from './components/SearchView';
import DisplayView from './components/DisplayView';



function App() {


  return (
    <div className="App">
      <BrowserRouter>

        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/display/:id' render={(props) => <DisplayView {...props} />} />
          <Route exact path='/search/:content' render={(props) => <SearchView {...props} />} />
          <Route component={Home} />
        </Switch>

      </BrowserRouter>
    </div>





  );
}

export default App;
