import { Card, Skeleton } from "antd";
import axios from "axios";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { RouteComponentProps } from "react-router-dom"

interface todoItem {

    id?: number,
    title: string,
    description: string,
    completed?: boolean
}
const SearchView = (props: RouteComponentProps<{ content: string; }>) => {
    const [todoItems, setTodoItems] = useState<todoItem[]>([])
    const [isLoading, setIsloading] = useState<boolean>(true);
    let history = useHistory()
    useEffect(() => {
        const content = props.match.params.content
        const fetchDataAsync = async () => {
            const res = await axios.get(`/api/todos/search/${content}`)
            setTodoItems(res.data)
            console.log(res.data)
            setIsloading(false);
        }

        fetchDataAsync()
    }, [])
    return (
        <>
            <h1 className="text-center text-3xl p-6 mt-10"> Search Results for "{props.match.params.content}"</h1>

            <Skeleton avatar title={true} loading={isLoading} active>
                <div style={{ width: "50%" }} className="mx-auto flex justify-around flex-wrap">
                    {todoItems.map(item => {
                        return (
                            <div onClick={() => history.push(`/display/${item.id}`)} className="w-56 mt-5 border-gray-500 birder-solid border-2">
                                <Card title={item.title} >
                                    {item.description}
                                </Card>
                            </div>

                        )
                    })}
                </div>
            </Skeleton>


        </>

    )
}

export default SearchView