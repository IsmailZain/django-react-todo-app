import React, { ChangeEventHandler, useEffect, useState } from 'react';

import { Avatar, Button, Input, Progress, Skeleton, Spin, Switch } from 'antd';
import { DeleteOutlined, DownloadOutlined, PlusOutlined, UnorderedListOutlined } from '@ant-design/icons';
import { List, Typography, Divider, Modal, } from 'antd';
import { Tabs } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import axios from "axios"
import Search from 'antd/lib/input/Search';
import { useHistory } from "react-router-dom";
import "./Home.css"
const { TabPane } = Tabs;


function callback(key: string) {
    console.log(key);
}



interface todoItem {

    id?: number,
    title: string,
    description: string,
    completed?: boolean
}

// const todoItems: todoItem[] = [
//   {
//     id: 1,
//     title: "Go to Market",
//     description: "Buy ingredients to prepare dinner",
//     completed: true,
//   },
//   {
//     id: 2,
//     title: "Study",
//     description: "Read Algebra and History textbook for the upcoming test",
//     completed: false,
//   },
//   {
//     id: 3,
//     title: "Sammy's books",
//     description: "Go to library to return Sammy's books",
//     completed: true,
//   },
//   {
//     id: 4,
//     title: "Article",
//     description: "Write article on how to Play football",
//     completed: false,
//   },
// ];

function Home() {
    let history = useHistory();
    const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
    const [todoItems, setTodoItems] = useState<todoItem[]>([])
    const [isLoading, setIsloading] = useState<boolean>(true);
    const [inpState, setInpstate] = useState<todoItem>({ title: "", description: "" })
    const [serach, setSearch] = useState<string>("")



    useEffect(() => {
        const fetchDataAsync = async () => {
            const res = await axios.get("/api/todos/")
            setTodoItems(res.data)
            console.log(res.data)
            setIsloading(false);
        }

        fetchDataAsync()

    }, [])
    const refreshList = () => {
        axios
            .get("/api/todos/")
            .then((res) => { setTodoItems(res.data); setIsloading(false) })
            .catch((err) => console.log(err));
    };


    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = async () => {

        // validations here
        setIsloading(true)
        await axios.post("/api/todos/", inpState)
            .then(res => refreshList())

        setInpstate({ title: "", description: "" })
        setIsModalVisible(false);
    };

    const handleDelete = (item: todoItem) => {
        setIsloading(true)
        axios
            .delete(`/api/todos/${item.id}/`)
            .then((res) => refreshList());
    };


    const handleCancel = () => {
        setIsModalVisible(false);
    };
    const handleToggleChecked = (e: boolean, item: todoItem) => {
        console.log(e)
        setIsloading(true)
        axios
            .put(`/api/todos/${item.id}/`, { ...item, completed: e })
            .then((res) => refreshList());

    }
    const handleSubmit = (e: string) => {
        history.push(`/search/${serach}`)
    }

    return (

        <div className="Home">
            <Spin tip="Loading..." spinning={isLoading}>
                <Modal title="Create a new Todo" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <Input value={inpState.title} name="title" placeholder="Add Title" onChange={(e) => { setInpstate({ ...inpState, title: e.target.value }) }} />
                    <TextArea value={inpState.description} name="description" placeholder="Add Description" onChange={(e) => { setInpstate({ ...inpState, description: e.target.value }) }} className="mt-5" showCount maxLength={500} />

                </Modal>
                <p className="ml-4 p-6 "><span className="font-medium text-2xl mr-2  inline-flex items-center"><UnorderedListOutlined className="mr-2 inline" /><span>ToDo List </span> </span>  Keep track of All of your work todo.<Search value={serach} onChange={(e) => setSearch(e.target.value)} onSearch={handleSubmit} className="inline mt-4" placeholder="Search for specific Todo" enterButton="Search" size="large" /> </p>
                <div className="flex">
                    <Button onClick={showModal} className="mx-auto w-100" type="primary" shape="round" icon={<PlusOutlined />}>ADD TASK</Button>
                </div>


                {/* List  */}
                <div className="p-10">
                    <Tabs defaultActiveKey="1" onChange={callback} >
                        <TabPane tab="All" key="1">
                            <Divider orientation="left">List of Todos</Divider>
                            <List
                                className="bg-white"

                                footer={<div>========================================================</div>}
                                bordered
                                dataSource={todoItems}
                                renderItem={item => (
                                    <List.Item>
                                        <Skeleton avatar title={true} loading={isLoading} active>
                                            {item.completed ? <Switch onChange={(e) => { handleToggleChecked(e, item) }} defaultChecked /> : <Switch onChange={(e) => { handleToggleChecked(e, item) }} />}

                                            <List.Item.Meta
                                                className="ml-4"
                                                title={<a onClick={() => history.push(`/display/${item.id}`)}>{item.title}</a>}
                                                description={item.description}
                                            />
                                            <div><DeleteOutlined onClick={e => { handleDelete(item) }} className=" p-3  rounded-full text-2xl hover:bg-red-600 hover:text-white" /></div>
                                        </Skeleton>
                                    </List.Item>
                                )}
                            />
                        </TabPane>
                        <TabPane tab="Completed" key="2">
                            <Divider orientation="left">List of Todos</Divider>
                            <List
                                className="bg-white"

                                footer={<div>========================================================</div>}
                                bordered
                                dataSource={todoItems.filter(item => item.completed)}
                                renderItem={item => (
                                    <List.Item>
                                        <Skeleton avatar title={true} loading={false} active>


                                            <List.Item.Meta
                                                className="ml-4"
                                                title={<a onClick={() => history.push(`/display/${item.id}`)}>{item.title}</a>}
                                                description={item.description}
                                            />
                                            <div><DeleteOutlined onClick={e => { handleDelete(item) }} className=" p-3  rounded-full text-2xl hover:bg-red-600 hover:text-white" /></div>
                                        </Skeleton>
                                    </List.Item>
                                )}
                            />
                        </TabPane>
                        <TabPane tab="InComplete" key="3">
                            <Divider orientation="left">List of Todos</Divider>
                            <List
                                className="bg-white"

                                footer={<div>========================================================</div>}
                                bordered
                                dataSource={todoItems.filter(item => !item.completed)}
                                renderItem={item => (
                                    <List.Item>
                                        <Skeleton avatar title={true} loading={false} active>


                                            <List.Item.Meta
                                                className="ml-4"
                                                title={<a onClick={() => history.push(`/display/${item.id}`)} >{item.title}</a>}
                                                description={item.description}
                                            />
                                            <div><DeleteOutlined onClick={e => { handleDelete(item) }} className=" p-3  rounded-full text-2xl hover:bg-red-600 hover:text-white" /></div>
                                        </Skeleton>
                                    </List.Item>
                                )}
                            />
                        </TabPane>
                    </Tabs>
                </div>


            </Spin>
        </div>

    );
}

export default Home;
