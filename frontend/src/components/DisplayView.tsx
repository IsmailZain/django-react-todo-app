import { useEffect, useState } from "react";
import { RouteComponentProps } from "react-router-dom"
import { Skeleton, Switch, Card, Avatar, Input } from 'antd';
import { ArrowRightOutlined, EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import TextArea from "antd/lib/input/TextArea";
import axios from "axios";
import { useHistory } from "react-router-dom";
import Item from "antd/lib/list/Item";

const { Meta } = Card;

interface todoItem {

    id?: number,
    title: string,
    description: string,
    completed?: boolean
}

const DisplayView = (props: RouteComponentProps<{ id: string; }>) => {
    let history = useHistory();
    const [todoItems, setTodoItems] = useState<todoItem>({ title: "", description: "" })
    const [isLoading, setIsloading] = useState<boolean>(true);
    const [inpState, setInpstate] = useState<todoItem>({ title: "", description: "" })

    useEffect(() => {
        const fetchDataAsync = async () => {
            const id = props.match.params.id
            const res = await axios.get(`/api/todos/${id}/`)
            setTodoItems(res.data)
            setInpstate(res.data)
            console.log(res.data)
            setIsloading(false);
        }

        fetchDataAsync()



    }, [])

    const handleSubmit = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
        setIsloading(true)
        const id = props.match.params.id
        axios
            .put(`/api/todos/${id}/`, inpState)
            .then((res) => { setTodoItems(res.data); history.push("/") })
            .catch((err) => console.log(err));

    }

    return (

        <div className="flex flex-col justify-center items-center min-h-screen w-100 ">
            <h1 className="text-5xl"> Edit ToDo</h1>
            <div style={{ width: "600px" }} className="border-black border-solid border-2 p-10">
                <Skeleton avatar title={true} loading={isLoading} active>
                    <h1>Title</h1>
                    <Input value={inpState.title} name="title" placeholder="Add Title" onChange={(e) => { setInpstate({ ...inpState, title: e.target.value }) }} />
                    <h1 className="mt-3">Description</h1>
                    <TextArea value={inpState.description} name="description" placeholder="Add Description" onChange={(e) => { setInpstate({ ...inpState, description: e.target.value }) }} className="mt-5" showCount maxLength={500} />
                    {inpState.completed ? <Switch defaultChecked onChange={e => { setInpstate({ ...inpState, completed: e }) }} /> : <Switch onChange={e => { setInpstate({ ...inpState, completed: e }) }} />}
                    <div className="flex justify-end">
                        <ArrowRightOutlined onClick={handleSubmit} className="text-4xl hover:text-white  hover:bg-green-500 rounded-full  mr-0  p-1" />
                    </div>

                </Skeleton>
            </div>




        </div>


    )
}

export default DisplayView