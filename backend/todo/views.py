from django.shortcuts import render
from .serializers import TodoSerializer
from rest_framework import viewsets
from .models import Todo
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from django.http import HttpResponse, JsonResponse
from django.db.models import Q
# Create your views here.


class TodoView(viewsets.ModelViewSet):
    serializer_class = TodoSerializer
    queryset = Todo.objects.all()


@ api_view(['GET'])
def TodoSearch(request, content):
    # content = request.GET.get("content")'

    todos = Todo.objects.filter(
        Q(title__contains=content) | Q(description__contains=content))
    serializer = TodoSerializer(todos, many=True)
    return Response(serializer.data)
